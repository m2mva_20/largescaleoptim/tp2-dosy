#!/usr/bin/python

import warnings

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd
import tqdm
from scipy.special import lambertw

warnings.filterwarnings('error')

Tmin = 1
Tmax = 1000
tmin = 0
tmax = 1.5
M = 50

def initProblem(data, show = True, plot = True):
    T = np.zeros(data.size)
    for i in range(1,data.size):
        T[i] = Tmin*np.exp(-(i-1)*np.log(Tmin/Tmax)/(data.size-1))
    t = np.zeros(M)
    for i in range(1,M):
        t[i] = tmin + (i-1)/(M-1)*(tmax-tmin)
    K = np.zeros([M, data.size])
    for m in range (M):
        for n in range(data.size):
            K[m,n] = np.exp(-T[n]*t[m])
    z = K.dot(data)
    sigma = 0.01*z[0]
    y = z + rd.normal(0, sigma, M)
    if plot:
        plt.figure()
        plt.plot(T, data)
        plt.grid()
        plt.xscale('log')
        plt.ylabel(r'original signal')
        plt.savefig('fig/1_originalSignal.pdf')
        plt.figure()
        plt.plot(t, y)
        plt.grid()
        plt.ylabel(r'Pulse gradient intensity measured')
        plt.xlabel(r't')
        plt.xlim([0, 1.5])
        plt.savefig('fig/1_noisyDataSimulation.pdf')
        if show:
            plt.show()
    return T, t, y, K

def plot(T, X, data, beta, mse, regLabel = ""):
    plt.figure()
    plt.plot(T, X, label='Reconstructed diffusin distribution')
    plt.plot(T,data, label = 'Real diffusion distribution')
    plt.grid()
    plt.legend()
    plt.xlabel(r'T')
    plt.xscale('log')
    plt.title('Recontruction with ' + regLabel + '\n' + r'$\beta = {}$ and $MSE = {:.2}\%$'.format(beta, str(mse*100)))
    plt.savefig('fig/reg_{}.pdf'.format(regLabel))
    plt.show()


def plotBetaError(x, data, title):
    print("plotting the error in function of beta ...")
    listBeta = [2**i for i in range(-12, 0)]
    plt.figure()
    plt.plot(listBeta, [error(x(b),data) for b in listBeta])
    plt.yscale('log')
    plt.grid()
    plt.xlabel(r'$\beta$')
    plt.ylabel('MSE')
    plt.title(r'Influence of $\beta$ over the estimation error')
    plt.savefig('fig/beta_' + title + '.pdf')
    plt.show()

def projC(x, xmin, xmax):
    r = np.zeros(x.size)
    for i in range(x.size):
        if x[i]>xmax:
            r[i] = xmax
        elif x[i]<xmin:
            r[i] = xmin
        else:
            r[i] = x[i]
    return r

def norme2(X):
    return X.T.dot(X)

def error(x, xTrue):
    if str(min(x))=='nan':
        return 2
    return norme2(x-xTrue)/norme2(xTrue)


def searchBeta(x, data):
    print("Searching for the value of beta ...")
    beta = min([2**i for i in range(-12, 0)], key = lambda b : error(x(b), data) )
    print("Beta found :")
    print("beta = ", beta)
    print('Calculating the normalised MSE ...')
    X = x(beta)
    mse = error(X, data)
    print("MSE found :")
    return X, beta, mse

def g1(x):
    N = x.size
    return 1/2*sum([(x[i%N] - x[(i-1)%N])**2 for i in range(N)])

def part1(T, t, y,K,data, numReg = 2, show=False):
    N = T.size
    D = np.eye(N)-np.roll(np.eye(N), N)
    if numReg==0:
        def x(beta):
            return np.linalg.inv(K.T.dot(K) + beta*D.T.dot(D)).dot(K.T).dot(y)
        X,beta, mse = searchBeta(x, data)
        print("Mean square error whith smoothness prior : ",mse  )
        if show:
            plotBetaError(x, data, title = 'smoothnessPrior')
            plot(T, X, data, beta, mse, regLabel="smoothness prior")
    elif numReg==1:
        nK = norme2(K)
        nD = norme2(D)
        nu =max(np.linalg.eigvals(K.T.dot(K) + D.T.dot(D))).real
        def x(beta, tol = 0.01):
            xmin = min(data)
            xmax = max(data)
            gamma = 0.99*2/nu
            lamb = 0.99*(2-nu*gamma/2)
            x = (xmin+xmax)/2 * np.ones(N)
            for i in tqdm.tqdm(range(9000)):
                u = x - gamma*(nK.dot(x) - K.T.dot(y) + beta*nD.dot(x))
                x = x + lamb * (projC(u, xmin, xmax)-x)
            return x
        X, beta, mse = searchBeta(x, data)
        print("Mean square error whith smoothness prior and constraints : ",mse  )
        if show:
            plotBetaError(x, data, title = 'smoothnessConstraints')
            plot(T, X, data, beta, mse, regLabel ="smoothnessConstraints" )
    elif numReg==2:
        nu =max(np.linalg.eigvals(K.T.dot(K))).real
        def x(beta):
            def prox(y, gamma, beta):
                return np.array([abs(yn)/yn * max(abs(yn)-gamma*beta, 0) for yn in y])
            gamma = 1.99/nu
            lamb = 0.9*(2-nu*gamma/2)
            x = (max(data)+min(data))/2*np.ones(N)
            for i in tqdm.tqdm(range(10000)):
                u = x - gamma*K.T.dot(K.dot(x)-y)
                x += lamb*(prox(u, gamma, beta)-x)
            return x
        nu = max(np.linalg.eigvals(K.T.dot(K))).real
        print('Parameter nu chosen : ')
        print('nu = ', nu)
        X, beta, mse = searchBeta(x, data)
        print("Mean square error whith sparsity prior : ",mse  )
        if show:
            plotBetaError(x, data, title = 'sparsityPrior')
            plot(T, X, data, beta, mse, regLabel="sparsityPrior")

def part2(T, t, y, K, data,solver=0, show=False):
    N = T.size
    kk = K.T.dot(K)
    nu =max(np.linalg.eigvals(K.T.dot(K))).real
    if solver == 0:
        def x(beta):
            gamma = 1.99/nu
            lamb = 0.9*(2-nu*gamma/2)
            gb = gamma*beta
            x = (max(data)+min(data))/2*np.ones(N)
            prox=lambda v:np.array([vn-gb*(1+np.log2(gb))-gb*np.log2(vn/gb-1-np.log2(gb)) if vn/gb>=1e2 else gb*lambertw(np.exp(vn/gb-1-np.log2(gb))).real for vn in v])
            for i in tqdm.tqdm(range(10000)):
                u = x - gamma*K.T.dot(K.dot(x) - y)
                x += lamb*(prox(u) - x)
            return x
        X, beta, mse = searchBeta(x, data)
        print("Mean square error with entropy regularisation forward backward : ", mse)
        if show:
            plotBetaError(x, data, title = 'entRegFB')
            plot(T, X, data, beta, mse, regLabel="entRegFB")
    elif solver == 1:
        def x(beta):
            gamma = 1
            lamb = 1.8
            gb = gamma*beta
            x = (max(data)+min(data))/2*np.ones(N)
            prox=lambda v:np.array([vn-gb*(1+np.log2(gb))-gb*np.log2(vn/gb-1-np.log2(gb)) if vn/gb>=1e2 else gb*lambertw(np.exp(vn/gb-1-np.log2(gb))).real for vn in v])
            for i in tqdm.tqdm(range(1000)):
                z = np.linalg.inv(gamma*kk + np.eye(kk.shape[0])).dot(gamma*K.T.dot(y) +2*prox(x)-x)
                if np.isnan(z).any():
                    print(gamma*K.T.dot(y) +2*prox(x)-x )
                    exit()#Die here and debug
                x += lamb*(z-prox(x))
            return prox(x)
        X, beta, mse = searchBeta(x, data)
        print("Mean square error with entropy regularisation Douglas Rachford : ", mse)
        if show:
            plotBetaError(x, data, title = 'entRegDR')
            plot(T, X, data, beta, mse, regLabel="entRegDR")

def main():
    data = np.loadtxt("x.txt").flatten()
    T, t, y, K = initProblem(data, plot = False)
    #part1(T,t,y, K, data, numReg=2, show=True)
    part2(T,t,y, K, data, solver = 1, show=True)




if __name__ == "__main__":
    main()
